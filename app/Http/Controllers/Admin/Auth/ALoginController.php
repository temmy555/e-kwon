<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class ALoginController extends Controller
{
    public function index(Request $request)
    {

//        $user = User::find(2);
//        $user->password = Hash::make('12345');
//        $user->save();
        return view('admin.menus.auth.login');
    }

    public function authenticate(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            return redirect('/');
        }
        $error = [
            'messages' => ['username atau password salah mohon dicek kembali!'],
        ];
        return redirect(route('login'))->withInput()->withErrors($error);
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('login'));
    }

}
