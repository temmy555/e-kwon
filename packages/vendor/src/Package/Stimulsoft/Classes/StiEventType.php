<?php

namespace Vendor\Package\Stimulsoft\Classes;

class StiEventType {
    const ExecuteQuery = "ExecuteQuery";
    const BeginProcessData = "BeginProcessData";
    //const EndProcessData = "EndProcessData";
    const CreateReport = "CreateReport";
    const OpenReport = "OpenReport";
    const SaveReport = "SaveReport";
    const SaveAsReport = "SaveAsReport";
    const PrintReport = "PrintReport";
    const BeginExportReport = "BeginExportReport";
    const EndExportReport = "EndExportReport";
    const EmailReport = "EmailReport";
    const DesignReport = "DesignReport";
}
