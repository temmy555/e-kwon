<?php

namespace Vendor\Package\Stimulsoft\Classes;

class StiDatabaseType {
    const XML = "XML";
    const JSON = "JSON";
    const MySQL = "MySQL";
    const MSSQL = "MS SQL";
    const PostgreSQL = "PostgreSQL";
    const Firebird = "Firebird";
    const Oracle = "Oracle";
}
