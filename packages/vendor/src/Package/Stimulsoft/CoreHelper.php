<?php

namespace Vendor\Package\Stimulsoft;

use stdClass;

class CoreHelper
{
    public $option;

    public static function createOptions()
    {
        $options = new stdClass();
        $options->handler = "handler.php";
        $options->timeout = 30;
        return $options;
    }

    public function setOptions($options)
    {
        $this->option = $options;
    }

    public function initialize($options = null)
    {
        if (isset($this->option)) $options = $this->option;
        if (!isset($options)) $options = CoreHelper::createOptions();
        ?>
        <script type="text/javascript">
            StiHelper.prototype.process = function (args, callback) {
                if (args) {
                    if (args.event == "BeginProcessData") {
                        args.preventDefault = true;
                        if (args.database == "XML" || args.database == "JSON" || args.database == "Excel") return callback(null);
                    }
                    var command = {};
                    for (var p in args) {
                        if (p == "report" && args.report != null) command.report = JSON.parse(args.report.saveToJsonString());
                        else if (p == "settings" && args.settings != null) command.settings = args.settings;
                        else if (p == "data") command.data = Stimulsoft.System.Convert.toBase64String(args.data);
                        else command[p] = args[p];
                    }

                    var isNullOrEmpty = function (value) {
                        return value == null || value === "" || value === undefined;
                    };
                    var json = JSON.stringify(command);
                    if (!callback) callback = function (message) {
                        if (Stimulsoft.System.StiError.errorMessageForm && !isNullOrEmpty(message)) {
                            var obj = JSON.parse(message);
                            if (!obj.success || !isNullOrEmpty(obj.notice)) {
                                var message = isNullOrEmpty(obj.notice) ? "There was some error" : obj.notice;
                                Stimulsoft.System.StiError.errorMessageForm.show(message, obj.success);
                            }
                        }
                    };
                    jsHelper.send(json, callback);
                }
            };

            StiHelper.prototype.send = function (json, callback) {
                try {
                    var request = new XMLHttpRequest();
                    request.open("post", this.url, true);
                    request.setRequestHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
                    request.setRequestHeader('Cache-Control', 'max-age=0');
                    request.setRequestHeader('Pragma', 'no-cache');
                    request.timeout = this.timeout * 1000;
                    request.onload = function () {
                        if (request.status == 200) {
                            var responseText = request.responseText;
                            request.abort();
                            callback(responseText);
                        } else {
                            Stimulsoft.System.StiError.showError("[" + request.status + "] " + request.statusText, false);
                        }
                    };
                    request.onerror = function (e) {
                        var errorMessage = "Connect to remote error: [" + request.status + "] " + request.statusText;
                        Stimulsoft.System.StiError.showError(errorMessage, false);
                    };
                    request.send(json);
                } catch (e) {
                    var errorMessage = "Connect to remote error: " + e.message;
                    Stimulsoft.System.StiError.showError(errorMessage, false);
                    request.abort();
                }
            };

            StiHelper.prototype.getUrlVars = function (json, callback) {
                var vars = {};
                var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
                    function (m, key, value) {
                        vars[key] = value;
                    });
                return vars;
            };

            function StiHelper(url, timeout) {
                this.url = url;
                this.timeout = timeout;
            }

            jsHelper = new StiHelper("<?php echo $options->handler; ?>", <?php echo $options->timeout; ?>);
        </script>
        <?php
    }

    public static function createHandler()
    {
        ?>
        jsHelper.process(arguments[0], arguments[1]);
        <?php
    }

    public function designer($data, $content, $jsonData)
    {
        $this->initialize();
        ?>
        <script type="text/javascript">
            const options = new Stimulsoft.Designer.StiDesignerOptions();
            options.appearance.fullScreenMode = true;
            options.showSendEmailButton = true;
            options.showFileMenuAbout = false;
            options.toolbar.showFileMenuAbout = false;
            options.toolbar.showAboutButton = false;
            options.toolbar.showFileMenu = false;

            const designer = new Stimulsoft.Designer.StiDesigner(options, "StiDesigner", false);

            designer.onBeginProcessData = function (event, callback) {
                {
                    <?php CoreHelper::createHandler();?>
                }
            };

            designer.onSaveReport = function (event) {
                {
                    <?php CoreHelper::createHandler();?>
                }
            };

            const dataSet = new Stimulsoft.System.Data.DataSet('source');
            dataSet.readJson(<?= $jsonData ?>);

            const report = new Stimulsoft.Report.StiReport();
            report.load(<?= $data ?>);

            report.dictionary.databases.clear();
            report.regData("source", "source", dataSet);
            report.dictionary.synchronize();

            designer.report = report;
            designer.renderHtml("<?= $content ?>");

            var toolBarRow = jsStiWebDesigner1.options.toolBar.firstChild.tr[0];

            //Add images for custom buttons to designer images collection
            jsStiWebDesigner1.options.images["ImageForCustomButton1"] = "https://www.stimulsoft.com/favicon.ico"; //For example

            var customButton = jsStiWebDesigner1.StatusPanelButton("customButton1", null, "ImageForCustomButton1", null, null, 30, 30);
            customButton.image.style.width = customButton.image.style.height = "16px";
            var buttonCell = document.createElement("td");
            buttonCell.className = "stiDesignerToolButtonCell";
            buttonCell.appendChild(customButton);

            toolBarRow.insertBefore(buttonCell, toolBarRow.childNodes[3]);

            customButton.action = function () {
                alert("Button clicked!");
            }
        </script>
        <?php
    }

    public function viewer($data, $content, $jsonData)
    {
        $this->initialize();
        ?>
        <script type="text/javascript">
            const options = new Stimulsoft.Viewer.StiViewerOptions();
            options.toolbar.showFullScreenButton = false;
            options.toolbar.showAboutButton = false;
            options.appearance.fullScreenMode = true;
            options.toolbar.printDestination = Stimulsoft.Viewer.StiPrintDestination.Pdf;
            options.toolbar.viewMode = Stimulsoft.Viewer.StiWebViewMode.Continuous;

            const viewer = new Stimulsoft.Viewer.StiViewer(options, "StiViewer", false);

            viewer.onBeginProcessData = function (event, callback) {
                {
                    <?php CoreHelper::createHandler();?>
                }
            };

            const dataSet = new Stimulsoft.System.Data.DataSet('source');
            dataSet.readJson(<?= $jsonData ?>);

            const report = new Stimulsoft.Report.StiReport();
            report.load(<?= $data ?>);

            report.dictionary.databases.clear();
            report.regData("source", "source", dataSet);
            report.dictionary.synchronize();

            viewer.report = report;
            viewer.renderHtml("<?= $content ?>");

        </script>
        <?php
    }
}
